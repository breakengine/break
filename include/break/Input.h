#pragma once

#include "break/Exports.h"

#include <break/platform-hal/Event.h>

#include <mn/Base.h>

#include <hml/Vector.h>

namespace brk
{
	MS_HANDLE(Input);

	API_BRK Input
	input_new();

	API_BRK void
	input_free(Input input);

	API_BRK void
	input_process_event(Input input, platform::Window_Event event);

	API_BRK bool
	input_mouse_up(Input input, platform::MOUSE button);

	API_BRK bool
	input_mouse_down(Input input, platform::MOUSE button);

	API_BRK bool
	input_mouse_pressed(Input input, platform::MOUSE button);

	API_BRK bool
	input_mouse_released(Input input, platform::MOUSE button);

	API_BRK bool
	input_key_up(Input input, platform::KEYBOARD key);

	API_BRK bool
	input_key_down(Input input, platform::KEYBOARD key);

	API_BRK bool
	input_key_pressed(Input input, platform::KEYBOARD key);

	API_BRK bool
	input_key_released(Input input, platform::KEYBOARD key);

	API_BRK hml::vec2i
	input_mouse_pos(Input input);

	API_BRK hml::vec2i
	input_mouse_delta(Input input);
}