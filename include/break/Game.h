#pragma once

#include <mn/Base.h>

namespace brk::platform
{
	//forward declare the window handle
	MS_FWD_HANDLE(Window);
}

namespace brk
{
	//forward declare the engine handle
	MS_FWD_HANDLE(Engine);

	struct IGame
	{
		virtual platform::Window init(Engine engine) = 0;
		virtual void setup(Engine engine) = 0;
		virtual void input() = 0;
		virtual bool update(double delta_micros) = 0;
		virtual void render() = 0;
	};
}