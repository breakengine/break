#pragma once

#include <hml/Graphics.h>

namespace brk
{
	struct CTransform
	{
		hml::vec3f position;
		hml::vec3f scale;
		hml::quatf rotation;
	};

	inline static CTransform
	ctransform_new()
	{
		return CTransform{
			hml::vec3f{},
			hml::vec3f{1.0f, 1.0f, 1.0f},
			hml::quatf{1.0f, 0.0f, 0.0f, 0.0f}
		};
	}

	inline static void
	ctranform_translate(CTransform& self, const hml::vec3f& v)
	{
		self.position += v;
	}

	inline static void
	ctransform_scale(CTransform& self, const hml::vec3f& v)
	{
		self.scale *= v;
	}

	inline static void
	ctransform_rotate(CTransform& self, const hml::quatf& q)
	{
		self.rotation *= q;
	}

	inline static hml::mat4f
	ctransform_mat4f(CTransform& self)
	{
		return hml::mat4f_transform(self.scale, self.rotation, self.position);
	}
}
