#pragma once

#include <hml/Graphics.h>

namespace brk
{
	struct CCamera_2D
	{
		hml::vec2f position;
		hml::vec2f scale;
		float rotation;
		float left, right, bottom, top;
	};

	inline static CCamera_2D
	ccamera_2d_new()
	{
		CCamera_2D self{};
		self.scale = hml::vec2f{ 1.0f, 1.0f };
		self.left = -1.0f;
		self.right = 1.0f;
		self.bottom = -1.0f;
		self.top = 1.0f;
		return self;
	}

	inline static void
	ccamera_2d_translate(CCamera_2D& self, const hml::vec2f& v)
	{
		self.position += v;
	}

	inline static void
	ccamera_2d_scale(CCamera_2D& self, const hml::vec2f& v)
	{
		self.scale *= v;
	}

	inline static void
	ccamera_2d_rotate(CCamera_2D& self, float angle)
	{
		self.rotation += angle;
	}

	inline static hml::mat4f
	ccamera_2d_view(const CCamera_2D& self)
	{
		return hml::mat4f_transform(
			hml::vec3f{ self.scale.x, self.scale.y, 1.0f },
			hml::quat_from_axis(hml::Z_AXIS, self.rotation),
			hml::vec3{self.position.x, self.position.y, 0.0f}
		);
	}

	inline static hml::mat4f
	ccamera_2d_proj(const CCamera_2D& self)
	{
		return hml::mat4f_ortho_2d(self.left, self.right, self.bottom, self.top);
	}

	inline static hml::mat4f
	ccamera_2d_viewproj(const CCamera_2D& self)
	{
		return ccamera_2d_proj(self) * ccamera_2d_view(self);
	}

	inline static hml::vec3f
	ccamera_2d_right(const CCamera_2D& self)
	{
		return hml::quat_from_axis(hml::Z_AXIS, self.rotation) * hml::X_AXIS;
	}

	inline static hml::vec3f
	ccamera_2d_up(const CCamera_2D& self)
	{
		return hml::quat_from_axis(hml::Z_AXIS, self.rotation) * hml::Y_AXIS;
	}
}
