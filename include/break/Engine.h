#pragma once

#include "break/Exports.h"
#include "break/Game.h"
#include "break/Input.h"

#include <break/platform-hal/Window.h>
#include <break/platform-gfx/Graphics_Backend.h>

#include <hml/Vector.h>

#include <mn/Base.h>

#include <stdint.h>

namespace brk::platform
{
	MS_FWD_HANDLE(Graphics);
}

namespace brk
{
	MS_HANDLE(Engine);

	//setup
	API_BRK Engine
	engine_new(platform::GRAPHICS_BACKEND backend);

	API_BRK void
	engine_free(Engine engine);

	API_BRK void
	engine_game(Engine engine, IGame* game);

	API_BRK void
	engine_run(Engine engine);

	//Graphics
	API_BRK platform::Graphics
	engine_graphics(Engine engine);

	//misc
	API_BRK uint32_t
	engine_fps(Engine engine);

	API_BRK hml::vec2i
	engine_window_size(Engine engine);

	//input handling
	API_BRK Input
	engine_input(Engine engine);
}
