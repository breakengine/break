#pragma once

#if defined(OS_WINDOWS)
	#if defined(BRK_DLL)
		#define API_BRK __declspec(dllexport)
	#else
		#define API_BRK __declspec(dllimport)
	#endif
#elif defined(OS_LINUX)
	#define API_BRK 
#endif