#pragma once

#include "break/Exports.h"

#include <hml/Vector.h>

#include <mn/Base.h>

namespace brk
{
	MS_HANDLE(Renderer_Point);

	MS_FWD_HANDLE(Engine);

	API_BRK Renderer_Point
	renderer_point_new(Engine engine);

	API_BRK void
	renderer_point_free(Renderer_Point r);

	inline static void
	destruct(Renderer_Point r)
	{
		renderer_point_free(r);
	}

	API_BRK void
	renderer_point_render(Renderer_Point r, const hml::vec3f& p);

	API_BRK void
	renderer_point_flush(Renderer_Point r);
}
