mn = path.getabsolute("external/mn")
hml = path.getabsolute("external/hamilton")
ecs = path.getabsolute("external/ecs")
platform_hal = path.getabsolute("external/platform/platform-hal")
platform_gfx = path.getabsolute("external/platform/platform-gfx")
loom = path.getabsolute("external/loom")
json = path.getabsolute("external/json")
fiz = path.getabsolute("external/fiz")

workspace "break"
	configurations {"debug", "release"}
	platforms {"x86", "x64"}
	location "build"
	targetdir "bin/%{cfg.platform}/%{cfg.buildcfg}/"
	startproject "scratch"
	defaultplatform "x64"

	group "External"
		include "external/mn/mn"
		include "external/hamilton/hml"
		include "external/ecs/ecs"
		include "external/platform/platform-hal"
		include "external/platform/platform-gfx"
		include "external/loom/loom"
		include "external/json/json"
		include "external/fiz/fiz"

	group ""

	include "break"
	include "scratch"