#include <mn/IO.h>
#include <mn/OS.h>

#include <break/Engine.h>
#include <break/Game.h>
#include <break/Input.h>
#include <break/renderer/Renderer_Point.h>

#include <break/platform-hal/Window.h>
#include <break/platform-hal/Cmd.h>

#include <break/platform-gfx/Graphics.h>

using namespace mn;
using namespace hml;
using namespace brk;
using namespace brk::platform;

struct Example_Game: IGame
{
	Engine engine;
	Window win;
	Input input_man;
	Renderer_Point p;

	Window
	init(Engine e) override
	{
		engine = e;

		input_man = engine_input(engine);

		win = window_new(800, 600, "Example Game");
		return win;
	}

	void
	setup(Engine e) override
	{
		p = renderer_point_new(e);
	}

	void
	input() override
	{
		//do nothing
		if(input_key_down(input_man, KEYBOARD::SPACE))
			printfmt("space down\n");

		if(input_key_pressed(input_man, KEYBOARD::SPACE))
			printfmt("space pressed\n");

		if(input_key_released(input_man, KEYBOARD::SPACE))
			printfmt("space released\n");
	}

	bool
	update(double delta_micros) override
	{
		printfmt("FPS: {}, delta: {}micros\n", engine_fps(engine), delta_micros);

		renderer_point_render(p, vec3f{});
		for(size_t i = 0; i < 5000; ++i)
			renderer_point_render(p, vec3f{(float)rand()/RAND_MAX, (float)rand()/RAND_MAX, (float)rand()/RAND_MAX});

		//return whether you force a frame to be rendered now
		return false;
	}

	void
	render() override
	{
		auto gfx = engine_graphics(engine);

		renderer_point_flush(p);

		Cmd c = graphics_cmd_new(gfx);
			cmd_window_use(c, win);
			cmd_clear_color(c, Color{0.0f, 0.0f, 0.0f, 1.0f});
		graphics_cmd_submit(gfx, c, -1);

		c = graphics_cmd_new(gfx);
			cmd_present(c);
		graphics_cmd_submit(gfx, c, 1);

		graphics_cmd_flush(gfx);
	}
};

int main()
{
	allocator_push(leak_detector());
	Example_Game game;
	Engine engine = engine_new(GRAPHICS_BACKEND::DX11);
	engine_game(engine, &game);
	engine_run(engine);
	engine_free(engine);
	return 0;
}