project "scratch"
	language "C++"
	kind "ConsoleApp"

	files 
	{
		"**.cpp",
		"**.h"
	}

	includedirs
	{
		"%{mn}/include",
		"%{hml}/include",
		"%{platform_hal}/include",
		"%{platform_gfx}/include",
		"../include"
	}

	links
	{
		"mn",
		"platform-hal",
		"platform-gfx",
		"break"
	}

	cppdialect "c++17"
	systemversion "latest"

	filter "system:linux"
		defines { "OS_LINUX" }

	filter "system:windows"
		defines { "OS_WINDOWS" }