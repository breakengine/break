#include "break/Input.h"

#include <mn/Memory.h>

// input handling and state happens here
namespace brk
{
	using namespace mn;
	using namespace hml;
	using namespace brk::platform;

	struct Input_State
	{
		vec2i mouse_pos;
		bool mouse[(uint8_t)MOUSE::COUNT];
		bool keyboard[(uint8_t)KEYBOARD::COUNT];
	};

	struct Internal_Input
	{
		Input_State current;
		Input_State previous;
	};

	Input
	input_new()
	{
		Internal_Input* self = alloc_zerod<Internal_Input>();
		return (Input)self;
	}

	void
	input_free(Input input)
	{
		Internal_Input* self = (Internal_Input*)input;
		free(self);
	}

	void
	input_process_event(Input input, Window_Event event)
	{
		Internal_Input* self = (Internal_Input*)input;
		self->previous = self->current;
		switch (event.kind)
		{
			case Window_Event::KIND::KIND_KEYBOARD_KEY:
			{
				uint8_t key = (uint8_t)event.keyboard.key;
				if (event.keyboard.state == KEY_STATE::DOWN)
					self->current.keyboard[key] = true;
				else if (event.keyboard.state == KEY_STATE::UP)
					self->current.keyboard[key] = false;
				break;
			}

			case Window_Event::KIND::KIND_MOUSE_BUTTON:
			{
				uint8_t button = (uint8_t)event.mouse.button;
				if (event.mouse.state == KEY_STATE::DOWN)
					self->current.mouse[button] = true;
				else if (event.mouse.state == KEY_STATE::UP)
					self->current.mouse[button] = false;
				break;
			}

			case Window_Event::KIND::KIND_MOUSE_MOVE:
			{
				self->current.mouse_pos = vec2i{ event.mouse_move.x, event.mouse_move.y };
				break;
			}

			default:
				break;
		}
	}

	bool
	input_mouse_up(Input input, MOUSE button)
	{
		Internal_Input* self = (Internal_Input*)input;
		return self->current.mouse[(uint8_t)button] == false;
	}

	bool
	input_mouse_down(Input input, MOUSE button)
	{
		Internal_Input* self = (Internal_Input*)input;
		return self->current.mouse[(uint8_t)button] == true;
	}

	bool
	input_mouse_pressed(Input input, MOUSE button)
	{
		Internal_Input* self = (Internal_Input*)input;
		return (self->current.mouse[(uint8_t)button] &&
				!self->previous.mouse[(uint8_t)button]);
	}

	bool
	input_mouse_released(Input input, MOUSE button)
	{
		Internal_Input* self = (Internal_Input*)input;
		return (!self->current.mouse[(uint8_t)button] &&
				self->previous.mouse[(uint8_t)button]);
	}

	bool
	input_key_up(Input input, KEYBOARD key)
	{
		Internal_Input* self = (Internal_Input*)input;
		return self->current.keyboard[(uint8_t)key] == false;
	}

	bool
	input_key_down(Input input, KEYBOARD key)
	{
		Internal_Input* self = (Internal_Input*)input;
		return self->current.keyboard[(uint8_t)key] == true;
	}

	bool
	input_key_pressed(Input input, KEYBOARD key)
	{
		Internal_Input* self = (Internal_Input*)input;
		return (self->current.keyboard[(uint8_t)key] &&
				!self->previous.keyboard[(uint8_t)key]);
	}

	bool
	input_key_released(Input input, KEYBOARD key)
	{
		Internal_Input* self = (Internal_Input*)input;
		return (!self->current.keyboard[(uint8_t)key] &&
				self->previous.keyboard[(uint8_t)key]);
	}

	vec2i
	input_mouse_pos(Input input)
	{
		Internal_Input* self = (Internal_Input*)input;
		return self->current.mouse_pos;
	}

	vec2i
	input_mouse_delta(Input input)
	{
		Internal_Input* self = (Internal_Input*)input;
		return self->current.mouse_pos - self->previous.mouse_pos;
	}
}