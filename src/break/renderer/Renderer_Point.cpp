#include "break/renderer/Renderer_Point.h"
#include "break/Engine.h"

#include <break/platform-gfx/Graphics.h>

#include <mn/Buf.h>
#include <mn/Memory.h>
#include <mn/Thread.h>

namespace brk
{
	using namespace mn;
	using namespace hml;
	using namespace brk::platform;

	constexpr static size_t BATCH_SIZE = 10000;

	struct IR_Point
	{
		Graphics gfx;

		Buf<vec3f> public_positions, private_positions;
		Mutex mtx;

		Buffer position;
		Geometry geometry;
		Pipeline pipeline;
		Program program;
	};

	Renderer_Point
	renderer_point_new(Engine engine)
	{
		IR_Point* self = alloc<IR_Point>();

		self->gfx = engine_graphics(engine);

		self->public_positions = buf_new<vec3f>();
		buf_reserve(self->public_positions, BATCH_SIZE);
		self->private_positions = buf_new<vec3f>();
		buf_reserve(self->private_positions, BATCH_SIZE);
		self->mtx = mutex_new("Renderer Point Mutex");


		self->position = graphics_buffer_new(self->gfx, BUFFER_TYPE::VERTEX, USAGE::DYNAMIC,
			Block{self->public_positions.ptr, self->public_positions.cap * sizeof(vec3f)});

		Buffer_Layout buffer_layout = buffer_layout_new({
			input_buffer_new(self->position, INPUT_TYPE::VEC3F)
		});
		self->geometry = graphics_geometry_new(self->gfx, buffer_layout);

		Input_Layout input_layout = input_layout_new({
			{SEMANTIC::POSITION, INPUT_TYPE::VEC3F}
		});
		self->pipeline = graphics_pipeline_new(self->gfx, input_layout);

		const char* vs = R"VS(
struct Vertex_Input
{
	float3 position: POSITION;
};

struct Vertex_Output
{
	float4 position: SV_POSITION;
};

Vertex_Output main(Vertex_Input input)
{
	Vertex_Output output;
	output.position = float4(input.position, 1.0f);
	return output;
}
)VS";

	const char* ps = R"PS(
struct Vertex_Output
{
	float4 position: SV_POSITION;
};

float4 main(Vertex_Output input): SV_TARGET
{
	return float4(1.0f, 1.0f, 1.0f, 1.0f);
}
)PS";

		self->program = graphics_program_new(self->gfx, input_layout, str_lit(vs), str_lit(ps));

		return (Renderer_Point)self;
	}

	void
	renderer_point_free(Renderer_Point r)
	{
		IR_Point* self = (IR_Point*)r;

		buf_free(self->public_positions);
		buf_free(self->private_positions);
		mutex_free(self->mtx);

		free(self);
	}

	void
	renderer_point_render(Renderer_Point r, const vec3f& p)
	{
		IR_Point* self = (IR_Point*)r;

		mutex_lock(self->mtx);
			buf_push(self->public_positions, p);
		mutex_unlock(self->mtx);
	}

	void
	renderer_point_flush(Renderer_Point r)
	{
		IR_Point* self = (IR_Point*)r;

		mutex_lock(self->mtx);
			std::swap(self->private_positions, self->public_positions);
			buf_clear(self->public_positions);
		mutex_unlock(self->mtx);

		size_t count = BATCH_SIZE > self->private_positions.count ? self->private_positions.count : BATCH_SIZE;
		Block data{self->private_positions.ptr, BATCH_SIZE * sizeof(vec3f)};

		Cmd cmd = graphics_cmd_new(self->gfx);
			cmd_buffer_map_write(cmd, self->position, data);
			cmd_pipeline_use(cmd, self->pipeline);
			cmd_clear(cmd, CLEAR_FLAGS_COLOR | CLEAR_FLAGS_DEPTH);
			cmd_program_use(cmd, self->program);
			cmd_geometry_draw(cmd, self->geometry, PRIMITIVE::POINTS, count);
		graphics_cmd_submit(self->gfx, cmd, 0);
	}
}
