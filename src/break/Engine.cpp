#include "break/Engine.h"

#include <break/platform-gfx/Graphics.h>

#include <mn/Memory.h>
#include <mn/IO.h>

#include <chrono>
#include <assert.h>

namespace brk
{
	using namespace brk::platform;
	using namespace hml;
	using namespace mn;

	using Time_Point = std::chrono::high_resolution_clock::time_point;

	struct IEngine
	{
		GRAPHICS_BACKEND backend;

		//basic
		Window window;
		IGame* game;

		//Graphics Instance
		Graphics gfx;

		//time
		double delta; //input microseconds
		uint32_t fps;
		uint32_t frame_limit;
		bool unlimited_fps;
		bool engine_run;

		//input
		Input input;
	};

	Engine
	engine_new(GRAPHICS_BACKEND backend)
	{
		IEngine* self = alloc<IEngine>();

		self->backend = backend;

		//basic
		self->window = nullptr;
		self->game = nullptr;

		//Graphics Instance
		self->gfx = graphics_new(backend);

		//time
		self->delta = 0.0;
		self->fps = 0;
		self->frame_limit = 90;
		self->unlimited_fps = false;
		self->engine_run = false;

		//input
		self->input = input_new();

		return (Engine)self;
	}

	void
	engine_free(Engine engine)
	{
		IEngine* self = (IEngine*)engine;

		//dispose the graphics part of the window
		graphics_window_dispose(self->gfx, self->window);
		graphics_cmd_flush(self->gfx);

		//free the window
		window_free(self->window);

		//free the graphics Instance
		graphics_free(self->gfx);

		// free the input handler
		input_free(self->input);

		//free the engine itself
		free(self);
	}

	void
	engine_game(Engine engine, IGame* game)
	{
		IEngine* self = (IEngine*)engine;
		self->game = game;
		self->window = self->game->init(engine);
		graphics_window_init(self->gfx, self->window);
		self->game->setup(engine);
	}

	void
	engine_run(Engine engine)
	{
		IEngine* self = (IEngine*)engine;
		assert(self->window && "game must setup a window");

		self->engine_run = true;

		Time_Point current = std::chrono::high_resolution_clock::now();
		Time_Point prev = current;
		double acc = 0.0;
		double fps_acc = 0.0;
		double render_acc = 0.0f;
		size_t frame_counter = 0;
		size_t step_counter = 0;
		while(self->engine_run)
		{
			bool render = false;

			current = std::chrono::high_resolution_clock::now();
			self->delta = std::chrono::duration<double, std::micro>(current - prev).count();
			prev = current;

			acc += self->delta;
			fps_acc += self->delta;
			render_acc += self->delta;

			//reset frame counter per second to calc the FPS
			if(fps_acc >= 1000000.0)
			{
				self->fps = frame_counter;
				frame_counter = 0;
				fps_acc = 0.0;
				step_counter = 0;
			}

			//reset the render_acc when it reaches the frame time limit
			double frame_timelimit = 1000000.0 / (double)self->frame_limit;

			while(acc >= frame_timelimit && self->engine_run)
			{
				Window_Event event = window_poll(self->window);
				if(event.kind == Window_Event::KIND_WINDOW_CLOSE)
					self->engine_run = false;

				input_process_event(self->input, event);
				self->game->input();
				self->game->update(frame_timelimit);
				++step_counter;
				acc -= frame_timelimit;
			}

			render = (self->unlimited_fps) | (render_acc >= frame_timelimit);

			if(render)
			{
				self->game->render();
				++frame_counter;
				render_acc = 0;
			}
		}
	}

	Graphics
	engine_graphics(Engine engine)
	{
		IEngine* self = (IEngine*)engine;
		return self->gfx;
	}

	uint32_t
	engine_fps(Engine engine)
	{
		IEngine* self = (IEngine*)engine;
		return self->fps;
	}

	vec2i
	engine_window_size(Engine engine)
	{
		IEngine* self = (IEngine*)engine;
		return window_size(self->window);
	}

	Input
	engine_input(Engine engine)
	{
		IEngine* self = (IEngine*)engine;
		return self->input;
	}
}
